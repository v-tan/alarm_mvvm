package com.dineshql.quokkaalarm.views

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.dineshql.quokkaalarm.R
import com.dineshql.quokkaalarm.receivers.AlarmReceiver
import com.dineshql.quokkaalarm.room.Alarm
import com.dineshql.quokkaalarm.utils.ShowTime
import com.dineshql.quokkaalarm.viewModels.AlarmViewModel
import kotlinx.android.synthetic.main.activity_alarm_setting.*
import java.util.*


class AlarmSetting : AppCompatActivity(){


    lateinit var viewModel: AlarmViewModel


    lateinit var alarmManager:AlarmManager
    lateinit var alarmIntent: PendingIntent
    lateinit var calendar: Calendar


     private var hour: Int = 0
    private var minute: Int = 0


    private lateinit var alarmRingTone:Uri



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm_setting)


        viewModel = ViewModelProvider(this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application)).get(AlarmViewModel::class.java)


        setSupportActionBar(toolbar)

        calendar = Calendar.getInstance()










        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }


        alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val intent = Intent(this, AlarmReceiver::class.java)

        alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0)




    }







    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
            val data: Intent? = result.data
            val uri: Uri? = data!!.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI)

            if (uri != null) {
                alarmRingTone = uri
            }
            textRingTOne.text = uri.toString()

        }
    }
    fun ringToneList(view: View) {
        val currentTone: Uri = RingtoneManager.getActualDefaultRingtoneUri(
            this,
            RingtoneManager.TYPE_ALARM
        )
        val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_RINGTONE)
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone")
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, currentTone)
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false)
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
        resultLauncher.launch(intent)
    }

    fun startAlarmButton(view: View) {




        calendar.set(Calendar.HOUR_OF_DAY, hour)
        calendar.set(Calendar.MINUTE, minute)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        var time = calendar.timeInMillis


        if(System.currentTimeMillis() > time){
            time += 24 * 60 * 60 * 1000
        }


        startAlarm(time)






    }

    fun changeTime(view: View) {

        val timePickerListener:TimePickerDialog.OnTimeSetListener = TimePickerDialog.OnTimeSetListener { _, i, i2 ->
            hour = i
            minute = i2

            setTimeToTextView(hour,minute)

        }

        val timePickerDialog = TimePickerDialog(this, timePickerListener, hour, minute, false)
        timePickerDialog.setTitle("Select Time")

        timePickerDialog.show()

    }


    private fun startAlarm(time:Long){
        alarmManager.set(AlarmManager.RTC_WAKEUP, time, alarmIntent)


        val label = editTextLabel.text.toString()
        val repeat = textRepeat.text.toString()
        if(label.isNotEmpty()){
            viewModel.insertAlarm(Alarm(hour,minute,label,  repeat,  true))


        }else{
            viewModel.insertAlarm(Alarm(hour,minute,"Simple Alarm", repeat, true))


        }

        Toast.makeText(this, "Alarm Created Successfully", Toast.LENGTH_LONG).show()

        onBackPressed()
    }


    private fun setTimeToTextView(hour: Int, minute: Int) {

        val showTime = ShowTime(hour, minute)


        timePickera.text = showTime.perfectTimeView()




     }







}




