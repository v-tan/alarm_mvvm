package com.dineshql.quokkaalarm.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dineshql.quokkaalarm.adapters.AlarmAdapter
import com.dineshql.quokkaalarm.R
import com.dineshql.quokkaalarm.interfaces.DeleteListener
import com.dineshql.quokkaalarm.interfaces.SwitchListener
import com.dineshql.quokkaalarm.room.Alarm
import com.dineshql.quokkaalarm.viewModels.AlarmViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), DeleteListener, SwitchListener {

    lateinit var viewModel: AlarmViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerViewAlarm.layoutManager = LinearLayoutManager(this)
        val adapter = AlarmAdapter(this, this, this)
        recyclerViewAlarm.adapter = adapter

        viewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        ).get(AlarmViewModel::class.java)


        viewModel.allAlarm.observe(this, {list->
            list ?.let{
                adapter.updateList(list)
            }
        })

        addAlarm.setOnClickListener {
          val intent = Intent(this, AlarmSetting::class.java)
            startActivity(intent)
        }


    }

    override fun onLongPressListener(alarm: Alarm) {
        viewModel.deleteAlarm(alarm)
        Toast.makeText(this, "Alarm Deleted Successfully", Toast.LENGTH_LONG).show()

    }

    override fun onChange(alarm: Alarm, b: Boolean) {




        if(b){
            viewModel.updateAlarm(true, alarm.id)


        }else{

           viewModel.updateAlarm(false, alarm.id)



        }

    }




}