package com.dineshql.quokkaalarm.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import com.dineshql.quokkaalarm.R
import com.dineshql.quokkaalarm.interfaces.DeleteListener
import com.dineshql.quokkaalarm.interfaces.SwitchListener
import com.dineshql.quokkaalarm.room.Alarm
import com.dineshql.quokkaalarm.utils.ShowTime
import com.dineshql.quokkaalarm.views.AlarmSetting

class AlarmAdapter(private val context: Context, private val switchListener: SwitchListener, private val longClickListener: DeleteListener): RecyclerView.Adapter<AlarmAdapter.ViewHolder>() {


    val allAlarm = ArrayList<Alarm>()

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val textTime: TextView = itemView.findViewById(R.id.txtTime)

        val textRepeat: TextView = itemView.findViewById(R.id.txtRepeatation)

        val switch: SwitchCompat = itemView.findViewById(R.id.switchButton)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_alarm,parent,false))




        viewHolder.itemView.setOnClickListener {

            longClickListener.onLongPressListener(allAlarm[viewHolder.adapterPosition])
        }

        return viewHolder


    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val currentAlarm = allAlarm[position]
        val showTime = ShowTime(currentAlarm.hour, currentAlarm.minute)

        holder.textTime.text = showTime.perfectTimeView()



        holder.textRepeat.text = currentAlarm.repeat

        holder.switch.isChecked = currentAlarm.onOff


        holder.switch.setOnCheckedChangeListener { _, b ->
            if(b){
                switchListener.onChange(allAlarm[position], true)


            }else{
                switchListener.onChange(allAlarm[position], false)


            }

        }



    }

    fun updateList(newList:List<Alarm>){

        allAlarm.clear()
        allAlarm.addAll(newList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return allAlarm.size
    }

}