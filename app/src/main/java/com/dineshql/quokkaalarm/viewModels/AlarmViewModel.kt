package com.dineshql.quokkaalarm.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.dineshql.quokkaalarm.repositories.AlarmRepository
import com.dineshql.quokkaalarm.room.Alarm
import com.dineshql.quokkaalarm.room.AlarmDataBase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AlarmViewModel(application: Application) : AndroidViewModel(application){
     val allAlarm: LiveData<List<Alarm>>
    private val repository : AlarmRepository

    init {
        val dao = AlarmDataBase.getDatabase(application).getAlarmDao()
        repository = AlarmRepository(dao)
        allAlarm = repository.allAlarm
    }

    fun deleteAlarm(alarm: Alarm) = viewModelScope.launch (Dispatchers.IO){
        repository.delete(alarm)
    }

    fun insertAlarm(alarm: Alarm) = viewModelScope.launch (Dispatchers.IO){
        repository.insert(alarm)
    }


    fun updateAlarm(onOff:Boolean, id: Int) = viewModelScope.launch (Dispatchers.IO){
        repository.update(onOff, id)
    }
}