package com.dineshql.quokkaalarm.repositories

import androidx.lifecycle.LiveData
import com.dineshql.quokkaalarm.room.Alarm
import com.dineshql.quokkaalarm.room.AlarmDao

class AlarmRepository(private val alarmDao: AlarmDao) {

    val allAlarm: LiveData<List<Alarm>> = alarmDao.getAllNotes()

    suspend fun insert(notes: Alarm){
        alarmDao.insert(notes)
    }


    suspend fun delete(notes: Alarm){
        alarmDao.delete(notes)

    }


    suspend fun update(onOff: Boolean, id: Int){
        alarmDao.updateAlarm(onOff, id)

    }
}