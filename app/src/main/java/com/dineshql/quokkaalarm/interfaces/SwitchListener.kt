package com.dineshql.quokkaalarm.interfaces

import com.dineshql.quokkaalarm.room.Alarm

interface SwitchListener {
    fun onChange(alarm: Alarm, b: Boolean)
}