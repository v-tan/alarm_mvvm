package com.dineshql.quokkaalarm.interfaces

import com.dineshql.quokkaalarm.room.Alarm

interface DeleteListener {
    fun onLongPressListener(alarm: Alarm)
}