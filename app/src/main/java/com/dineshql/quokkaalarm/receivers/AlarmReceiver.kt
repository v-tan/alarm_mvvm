package com.dineshql.quokkaalarm.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.Ringtone
import android.media.RingtoneManager
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.content.ContentProviderCompat.requireContext


class AlarmReceiver: BroadcastReceiver() {

    override fun onReceive(p0: Context, p1: Intent?) {



        val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        val ringtoneSound: Ringtone = RingtoneManager.getRingtone(p0, ringtoneUri)
        ringtoneSound.play()




    }
}