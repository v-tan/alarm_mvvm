package com.dineshql.quokkaalarm.utils

class ShowTime(hour: Int, minute: Int) {

    val h = hour
    val m = minute



    // Returns  Single to Double Digits
    private fun singleToDoubleDigit(digit: String): String{
       val size: Int = digit.length

        return if(size == 1) "0$digit" else digit
    }

    // Change Integer to String
    private fun intToString(value: Int): String{
        return value.toString()
    }

    // Decides AM or PM
    private fun amOrPm(): String{
        return if(h >= 12) "PM" else "AM"
    }

    private fun getHour(): String{
        var _hour: Int = 0
        if(h == 0){
            _hour = 12
        }
        else if(h > 12){
            _hour = h - 12
        }

        return singleToDoubleDigit(intToString(_hour))
    }

    private fun getMinute(): String{

        return singleToDoubleDigit(intToString(m))

    }

    fun perfectTimeView(): String{
        var time: String = ""
        val nhour = getHour()
        val nminute = getMinute()
        val amOrPm = amOrPm()

        time  = "$nhour:$nminute $amOrPm"

        return time
    }

}