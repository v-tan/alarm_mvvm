package com.dineshql.quokkaalarm.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "alarm_table")
class Alarm(@ColumnInfo(name = "hour") val hour:Int,
            @ColumnInfo(name = "minute") val minute:Int,
            @ColumnInfo(name = "label") val label: String,
            @ColumnInfo(name = "repeat") val repeat: String,
            @ColumnInfo(name = "onOff") val onOff: Boolean)
{
    @PrimaryKey(autoGenerate = true) var id = 0
}