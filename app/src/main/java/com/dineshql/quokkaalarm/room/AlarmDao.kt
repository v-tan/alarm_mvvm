package com.dineshql.quokkaalarm.room

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface AlarmDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(notes: Alarm)

    @Delete
    suspend fun delete(notes: Alarm)

    @Query("Select * from alarm_table order by id ASC")
    fun getAllNotes(): LiveData<List<Alarm>>




    @Query("UPDATE alarm_table SET onOff = :onOff WHERE id =:id")
    fun updateAlarm(onOff: Boolean, id: Int)

}