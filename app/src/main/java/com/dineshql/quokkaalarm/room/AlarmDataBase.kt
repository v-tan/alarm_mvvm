package com.dineshql.quokkaalarm.room

import android.app.AlarmManager
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = arrayOf(Alarm::class), version = 2, exportSchema = false)
abstract class AlarmDataBase: RoomDatabase() {
    abstract fun getAlarmDao():AlarmDao


    companion object{

        @Volatile
        private var INSTANCE: AlarmDataBase? = null
        fun getDatabase(context: Context): AlarmDataBase{
            return INSTANCE?: synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AlarmDataBase::class.java,
                    "notes_database"
                ).build()
                INSTANCE = instance

                instance
            }
        }
    }
}




